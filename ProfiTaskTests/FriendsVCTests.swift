//
//  FriendsVCTests.swift
//  ProfiTaskTests
//
//  Created by Sher Locked on 30.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import XCTest
@testable import ProfiTask

class FriendsVCTests: XCTestCase {
    
    var vc: FriendsVC!
    let mockNetworkManager = MockNetworkManager()
    let mockUIConfigurator = MockUIConfigurator()
    let mockErrorHandler = MockErrorHandler()
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "FriendsVC", bundle: nil)
        vc = storyboard.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
        vc.loadView()
        vc.networkManager = mockNetworkManager
        vc.uiConfigurator = mockUIConfigurator
        vc.errorHandler = mockErrorHandler
    }
    
    func testViewDidLoad_ShouldLoadFirstData() {
        // when
        vc.viewDidLoad()
        
        // then
        XCTAssertTrue(mockNetworkManager.getFriendsCalled, "GetFriends didn't called")
    }
    
    func testLoadMoreFriends_ShouldCallNetworkManager() {
        // when
        vc.loadMoreFriends(start: 0, limit: 10) {}
        
        // then
        XCTAssertTrue(mockNetworkManager.getFriendsCalled, "GetFriends didn't called")
    }
    
    func testLoadMoreFriends_WhenError_ShouldHandleError() {
        //given
        mockNetworkManager.shouldReturnError = true
        // when
        vc.loadMoreFriends(start: 0, limit: 10) {}
        
        // then
        XCTAssertTrue(mockNetworkManager.getFriendsCalled, "GetFriends didn't called")
        XCTAssertTrue(mockErrorHandler.handleErrorCalled, "Error didnt handle")
    }
    
    func testLoadMoreFriends_ShouldUseCorrectData() {
        // given
        let start = 6
        let limit = 56
        let accessCode = "testAccessCode123"
        vc.accessCode = accessCode
        
        //when
        vc.loadMoreFriends(start: start, limit: limit) {}
        
        //then
        XCTAssertEqual(start, mockNetworkManager.lastOffset, "GetFriends called with incorrect offset")
        XCTAssertEqual(limit, mockNetworkManager.lastCount, "GetFriends called with incorrect count")
        XCTAssertEqual(accessCode, mockNetworkManager.lastAccessToken, "GetFriends called with incorrect token")
    }
    
    func testDidSelectRow_ShouldCreateOpenPhotoVC() {
        //given
        let mockFriend = Friend(surname: "Surname", name: "Name", smallPhotoURL: nil, largePhotoURL: nil)
        vc.friends = [mockFriend]
        
        //when
        vc.tableView(vc.tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        
        //then
        XCTAssertTrue(mockUIConfigurator.createOpenPhotoVCCalled, "Cant open OpenPhotoVC")
    }
    
    func testWillDisplayCell_WhenLastCell_ShouldLoadMoreData() {
        //given
        let cell = UITableViewCell(frame: .zero)
        vc.pagesLoaded = 1
        let indexPath = IndexPath(row: vc.startLoadingIndex - 1, section: 0)
        
        //when
        vc.tableView(vc.tableView, willDisplay: cell, forRowAt: indexPath)
        
        // then
        XCTAssertTrue(mockNetworkManager.getFriendsCalled, "GetFriends didn't called")
    }
    
    func testWillDisplayCell_WhenNotLastCell_ShouldntLoadMoreData() {
        //given
        let cell = UITableViewCell(frame: .zero)
        vc.pagesLoaded = 1
        let indexPath = IndexPath(row: vc.startLoadingIndex - 5, section: 0)
        
        //when
        vc.tableView(vc.tableView, willDisplay: cell, forRowAt: indexPath)
        
        // then
        XCTAssertFalse(mockNetworkManager.getFriendsCalled, "GetFriends called")
    }
    

    
}
