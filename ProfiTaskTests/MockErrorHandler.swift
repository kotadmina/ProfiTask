//
//  MockErrorHandler.swift
//  ProfiTaskTests
//
//  Created by Sher Locked on 01.05.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation
@testable import ProfiTask

class MockErrorHandler: ErrorHandlerProtocol {
    
    var handleErrorCalled = false
    
    func handleError(_ error: Error) {
        handleErrorCalled = true
    }
    
    
}
