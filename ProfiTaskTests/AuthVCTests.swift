//
//  AuthVCTests.swift
//  ProfiTaskTests
//
//  Created by Sher Locked on 01.05.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import XCTest

@testable import ProfiTask

class AuthVCTests: XCTestCase {
    
    var vc: AuthVC!
    let mockUIConfigurator = MockUIConfigurator()
    let mockNetworkManager = MockNetworkManager()
    
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "AuthVC", bundle: nil)
        vc = storyboard.instantiateViewController(withIdentifier: "AuthVC") as! AuthVC
        vc.uiConfigurator = mockUIConfigurator
        vc.networkManager = mockNetworkManager
        vc.loadView()
    }
    
    func testContinueWithAccessCode_ShouldCreateFriendsVC() {
        //when
        vc.continueWithAccessCode("testAccessCode")
        
        //then
        XCTAssertTrue(mockUIConfigurator.createFriendsVCCalled, "FriendsVC didnt open")
    }
    
    func testCheckPageForAccessCode_ShouldCheckAccessCode() {
        //given
        let url = URL(string: "google.com")!
        
        //when
        vc.checkPageForAccessCode(url: url)
        
        //then
        XCTAssertTrue(mockNetworkManager.getAccessCodeCalled, "Get access code didnt call")
    }
    
}
