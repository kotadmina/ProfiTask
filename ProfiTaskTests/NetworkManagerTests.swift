//
//  NetworkManagerTests.swift
//  ProfiTaskTests
//
//  Created by Sher Locked on 01.05.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import XCTest
@testable import ProfiTask

class NetworkManagerTests: XCTestCase {
    
    let networkManager = NetworkManager()
    
    func testGetAccessCode_WhenCorrectURL_ShouldReturnAccessCode() {
        //given
        let mockAccessToken = "12345"
        let url = URL(string: "https://oauth.vk.com/blank.html#access_token=\(mockAccessToken)")!
        //when
        let accessCode = networkManager.getAccessCode(from: url)
        
        //then
        XCTAssertEqual(accessCode, mockAccessToken, "Returned wrong access code")
    }
    
    func testGetAccessCode_WhenIncorrectURL_ShouldntReturnAccessCode() {
        //given
        let url = URL(string: "google.com")!
        //when
        let accessCode = networkManager.getAccessCode(from: url)
        
        //then
        XCTAssertNil(accessCode, "Returned Some access code but shouldnt")
    }
   
    
}
