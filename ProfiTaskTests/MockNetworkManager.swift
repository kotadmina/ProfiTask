//
//  MockNetworkManager.swift
//  ProfiTaskTests
//
//  Created by Sher Locked on 30.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation
@testable import ProfiTask

class MockNetworkManager: NetworkManagerProtocol {
    
    var getFriendsCalled = false
    var getAccessCodeCalled = false
    
    var lastCount: Int? = nil
    var lastOffset: Int? = nil
    var lastAccessToken: String? = nil
    
    var shouldReturnError = false
    
    func getFriendsList(count: Int, offset: Int, accessToken: String, completion: @escaping ([Friend], Error?) -> Void) {
        getFriendsCalled = true
        lastCount = count
        lastOffset = offset
        lastAccessToken = accessToken
        
        if shouldReturnError {
            let mockError = NSError(domain: "", code: 1, userInfo: nil)
            completion([], mockError)
        } else {
            let mockFriend = Friend(surname: "Surname", name: "Name", smallPhotoURL: nil, largePhotoURL: nil)
            let friendArray = [Friend](repeating: mockFriend, count: count)
            completion(friendArray, nil)
        }
        
    }
    
    func getAccessCode(from url: URL) -> String? {
        getAccessCodeCalled = true
        return ""
    }
    
    
}
