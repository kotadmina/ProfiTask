//
//  MockUIConfigurator.swift
//  ProfiTaskTests
//
//  Created by Sher Locked on 30.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
@testable import ProfiTask

class MockUIConfigurator: UIConfiguratorProtocol {
    
    var createFriendsVCCalled = false
    var createOpenPhotoVCCalled = false
    var createAuthVCCalled = false
    
    func createFriendsVC(with accessCode: String) -> UIViewController {
        createFriendsVCCalled = true
        return UIViewController()
    }
    
    func createOpenPhotoVC(with friend: Friend) -> UIViewController {
        createOpenPhotoVCCalled = true
        return UIViewController()
    }
    
    func createAuthVC() -> UIViewController {
        createAuthVCCalled = true
        return UIViewController()
    }
    
    
}
