//
//  ImageCache.swift
//  ProfiTask
//
//  Created by Sher Locked on 29.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class ImageCache {
    
    static let shared = ImageCache()
    private var cache: [URL: UIImage] = [:]
    
    func getImageFromCache(url: URL) -> UIImage? {
        return cache[url]
    }
    
    func setImageToCache(url: URL, image: UIImage) {
        cache[url] = image
    }
}
