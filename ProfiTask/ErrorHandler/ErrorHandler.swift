//
//  ErrorHandler.swift
//  ProfiTask
//
//  Created by Sher Locked on 01.05.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class ErrorHandler: ErrorHandlerProtocol {
    
    weak var vc: UIViewController?
    
    func handleError(_ error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(alertAction)
        vc?.present(alert, animated: true, completion: nil)
    }
    
}
