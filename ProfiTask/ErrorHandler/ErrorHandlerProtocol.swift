//
//  ErrorHandlerProtocol.swift
//  ProfiTask
//
//  Created by Sher Locked on 01.05.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol ErrorHandlerProtocol {
    func handleError(_ error: Error)
}
