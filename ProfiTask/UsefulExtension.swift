//
//  UsefulExtension.swift
//  ProfiTask
//
//  Created by Sher Locked on 29.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        self.image = nil
        self.contentMode = mode
        if let cachedImage = ImageCache.shared.getImageFromCache(url: url) {
            self.image = cachedImage
            return
        }
        let activityIndicator = UIActivityIndicatorView(frame: self.bounds)
        activityIndicator.color = UIColor.darkGray
        self.addSubview(activityIndicator)
        activityIndicator.startAnimating()

        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            ImageCache.shared.setImageToCache(url: url, image: image)
            DispatchQueue.main.async() {
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
                self.image = image
            }
        }
        task.resume()
    }
}
