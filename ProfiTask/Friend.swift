//
//  Friend.swift
//  ProfiTask
//
//  Created by Sher Locked on 27.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct Friend {
    var surname: String?
    var name: String?
    var smallPhotoURL: URL?
    var largePhotoURL: URL?
}
