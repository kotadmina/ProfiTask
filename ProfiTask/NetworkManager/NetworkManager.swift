//
//  NetworkManager.swift
//  ProfiTask
//
//  Created by Sher Locked on 28.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class NetworkManager: NetworkManagerProtocol {
    
    func getFriendsList(count: Int, offset: Int, accessToken: String, completion: @escaping ([Friend], Error?) -> Void) {
        
        let urlString = "https://api.vk.com/method/friends.get"
        guard let url = URL(string: urlString) else {
            DispatchQueue.main.async {
                completion([], nil)
            }
            return
        }
        
        let params: [String: String] = ["count": String(count),
                                        "offset": String(offset),
                                        "fields": "photo_50,photo_400_orig",
                                        "access_token": accessToken,
                                        "v": "5.74"]
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            DispatchQueue.main.async {
                completion([], nil)
            }
            return
        }
        
        components.queryItems = params.map({ (key, value) -> URLQueryItem in
            return URLQueryItem(name: key, value: value)
        })
        
        guard let urlWithParams = components.url else {
            DispatchQueue.main.async {
                completion([], nil)
            }
            return
        }
        
        let request = URLRequest(url: urlWithParams)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                DispatchQueue.main.async {
                    completion([], error)
                }
                return
            }
            let responseObject = (try? JSONSerialization.jsonObject(with: data)) as? [String: Any]
            let responseDict = responseObject?["response"] as? [String: Any]
            let responseArray = (responseDict?["items"] as? [[String: Any]]) ?? []
            let friends: [Friend] = responseArray.map({ (dict) -> Friend in
                let name = dict["first_name"] as? String
                let surname = dict["last_name"] as? String
                var smallPhotoURL: URL?
                if let smallPhoto = dict["photo_50"] as? String {
                    smallPhotoURL = URL(string: smallPhoto)
                }
                var largePhotoURL: URL?
                if let largePhoto = dict["photo_400_orig"] as? String {
                    largePhotoURL = URL(string: largePhoto)
                }
                return Friend(surname: surname, name: name, smallPhotoURL: smallPhotoURL, largePhotoURL: largePhotoURL)
            })
            DispatchQueue.main.async {
                completion(friends, nil)
            }
        }
        
        task.resume()
    }
    
    func getAccessCode(from url: URL) -> String? {
        if url.host == Constants.vkAuthHost, let fragment = url.fragment {
            let params = fragment.components(separatedBy: "&")
            for param in params {
                let components = param.components(separatedBy: "=")
                if components.count == 2 && components.first == Constants.vkAccessCodeKey {
                    let accessCode = components[1]
                    return accessCode
                }
            }
        }
        return nil
    }
    
}
