//
//  NetworkManagerProtocol.swift
//  ProfiTask
//
//  Created by Sher Locked on 30.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol NetworkManagerProtocol {
    
    func getFriendsList(count: Int, offset: Int, accessToken: String, completion: @escaping ([Friend], Error?) -> Void)
    func getAccessCode(from url: URL) -> String?

}
