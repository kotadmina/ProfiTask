//
//  FriendsCell.swift
//  ProfiTask
//
//  Created by Sher Locked on 28.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class FriendsCell: UITableViewCell {
    
    static var cellHeight: CGFloat = 98

    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photo: UIImageView!
    
    func configure(with friend: Friend) {
        surnameLabel.text = friend.surname
        nameLabel.text = friend.name
        if let url = friend.smallPhotoURL {
            photo.downloadedFrom(url: url)
        }
    }
}
