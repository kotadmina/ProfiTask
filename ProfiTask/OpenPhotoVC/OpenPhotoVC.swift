//
//  OpenPhotoVC.swift
//  ProfiTask
//
//  Created by Sher Locked on 29.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class OpenPhotoVC: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var largePhoto: UIImageView!
    
    var friend: Friend?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.maximumZoomScale = 6
        scrollView.minimumZoomScale = 1
        scrollView.delegate = self
        if let url = friend?.largePhotoURL {
            largePhoto.downloadedFrom(url: url)
        }
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension OpenPhotoVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return largePhoto
    }
}
