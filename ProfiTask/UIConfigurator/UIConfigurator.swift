//
//  UIConfigurator.swift
//  ProfiTask
//
//  Created by Sher Locked on 30.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class UIConfigurator: UIConfiguratorProtocol {
    
    func createAuthVC() -> UIViewController {
        let storyboard = UIStoryboard(name: "AuthVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AuthVC") as! AuthVC
        vc.networkManager = NetworkManager()
        vc.uiConfigurator = self
        return vc
    }
    
    func createFriendsVC(with accessCode: String) -> UIViewController {
        let storyboard = UIStoryboard(name: "FriendsVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
        vc.accessCode = accessCode
        vc.networkManager = NetworkManager()
        let errorHandler = ErrorHandler()
        vc.errorHandler = errorHandler
        errorHandler.vc = vc
        vc.uiConfigurator = self
        return vc
    }
    
    func createOpenPhotoVC(with friend: Friend) -> UIViewController {
        let storyboard = UIStoryboard(name: "OpenPhotoVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OpenPhotoVC") as! OpenPhotoVC
        vc.friend = friend
        return vc
    }
    
    
}
