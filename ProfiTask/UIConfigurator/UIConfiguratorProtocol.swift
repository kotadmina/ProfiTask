//
//  UIConfiguratorProtocol.swift
//  ProfiTask
//
//  Created by Sher Locked on 30.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

protocol UIConfiguratorProtocol {
    
    func createFriendsVC(with accessCode: String) -> UIViewController
    func createOpenPhotoVC(with friend: Friend) -> UIViewController
    func createAuthVC() -> UIViewController
}
