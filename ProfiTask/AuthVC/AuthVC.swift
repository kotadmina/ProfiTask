//
//  AuthVC.swift
//  ProfiTask
//
//  Created by Sher Locked on 27.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import WebKit

class AuthVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    var uiConfigurator: UIConfiguratorProtocol!
    var networkManager: NetworkManagerProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        guard let url = URL(string: Constants.vkAuthURL) else {
            return
        }
        let request = URLRequest(url: url)
        webView.load(request)
    }
    
    func continueWithAccessCode(_ accessCode: String) {
        let vc = uiConfigurator.createFriendsVC(with: accessCode)
        present(vc, animated: true, completion: nil)
    }

    func checkPageForAccessCode(url: URL) {
        if let accessCode = networkManager.getAccessCode(from: url) {
            continueWithAccessCode(accessCode)
        }
    }

}

extension AuthVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            checkPageForAccessCode(url: url)
        }
        decisionHandler(.allow)
    }
    
}
