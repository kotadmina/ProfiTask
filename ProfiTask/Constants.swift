//
//  Constants.swift
//  ProfiTask
//
//  Created by Sher Locked on 27.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct Constants {
    static let vkAuthURL = "https://oauth.vk.com/authorize?client_id=6463176&display=mobile&response_type=token&redirect_uri=https://oauth.vk.com/blank.html&v=5.74"
    static let vkAuthHost = "oauth.vk.com"
    static let vkAccessCodeKey = "access_token"
    
}
