//
//  FriendsVC.swift
//  ProfiTask
//
//  Created by Sher Locked on 27.04.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class FriendsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var accessCode: String = ""
    
    var friends: [Friend] = []
    
    var pagesLoaded = 0
    let pageSize = 30
    
    var networkManager: NetworkManagerProtocol!
    var uiConfigurator: UIConfiguratorProtocol!
    var errorHandler: ErrorHandlerProtocol!
    
    var startLoadingIndex: Int {
        return pagesLoaded * pageSize
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "FriendsCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "FriendsCell")
        tableView.separatorStyle = .none
        loadMoreFriends(start: startLoadingIndex, limit: pageSize) {
            self.pagesLoaded += 1
            self.updateTable()
        }
    }
    
    func loadMoreFriends(start: Int, limit: Int, completion: @escaping () -> Void) {
        networkManager.getFriendsList(count: limit, offset: start, accessToken: accessCode) { (friends, error) in
            if let error = error {
                self.errorHandler.handleError(error)
                completion()
                return
            }
            self.friends.append(contentsOf: friends)
            completion()
        }
    }
    
    func updateTable() {
        tableView.reloadData()
    }
}

extension FriendsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsCell") as! FriendsCell
        let friend = friends[indexPath.row]
        cell.configure(with: friend)
        return cell
    }
}

extension FriendsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FriendsCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let friend = friends[indexPath.row]
        let vc = uiConfigurator.createOpenPhotoVC(with: friend)
        present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == startLoadingIndex - 1 {
            loadMoreFriends(start: startLoadingIndex, limit: pageSize) {
                self.pagesLoaded += 1
                self.updateTable()
            }
        }
    }
    
}
